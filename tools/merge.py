#!/usr/bin/env python3

import argparse
import os
from pathlib import Path

from utils.yaml import load_meta, yaml_dump


def merge(source, dest, fields):
    print('Merging {} from {} -> {}'.format(fields, source, dest))

    with source.open() as f:
        source_meta = load_meta(f)

    with dest.open() as f:
        dest_meta = load_meta(f)

    source_by_video = {}
    for event in source_meta.videos:
        source_by_video[event.video] = event

    for event in dest_meta.videos:
        source_event = source_by_video.get(event.video)
        if not source_event:
            continue
        for field in fields:
            setattr(event, field, getattr(source_event, field))

    tmp_path = dest.with_name('.new')
    with tmp_path.open('w', encoding='utf8') as f:
        try:
            yaml_dump(dest_meta, f)
            f.flush()
            os.fsync(f.fileno())
            os.replace(str(tmp_path), str(dest))
        finally:
            if tmp_path.exists():
                tmp_path.unlink()


def main():
    parser = argparse.ArgumentParser(
        description='Merge new fields into existing data')
    parser.add_argument('source', help='Source file, with the new fields')
    parser.add_argument('dest', help='Destination file, which is canonical')
    parser.add_argument('-f', '--fields', action='append',
                        help='Fields to merge across')
    args = parser.parse_args()

    source = Path(args.source)
    if not source.exists():
        parser.error('{} does not exist'.format(source))

    dest = Path(args.dest)
    if not dest.exists():
        parser.error('{} does not exist'.format(dest))

    if not args.fields:
        parser.error('Must specify a field')

    merge(source, dest, args.fields)


if __name__ == '__main__':
    main()
