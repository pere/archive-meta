#!/usr/bin/env python3

import argparse
import os
import re
import xml.etree.ElementTree as ET
from datetime import date, datetime
from pathlib import Path
from urllib.parse import urljoin

import requests

from utils.yaml import load_meta_from_dict, yaml_dump


def parse_date(datestr):
    return date(*[int(part) for part in datestr.split('-')])


def parse_datetime(datestr):
    if '.' in datestr:
        return datetime.strptime(
            datestr, '%Y-%m-%d %H:%M:%S.%f+00').replace(microsecond=0)
    return datetime.strptime(datestr, '%Y-%m-%d %H:%M:%S+00')


def split_speakers(speakers):
    speakers = ','.join(speakers)
    for speaker_or_two in speakers.split(','):
        for speaker in speaker_or_two.split(' and '):
            yield speaker.strip()


def read_dc_schedule(url, eventid_value):
    r = requests.get(url)
    schedule = ET.fromstring(r.content)
    by_id = {}
    event_id_re = re.compile(r'^/talks/(\d+)-.*')
    for event in schedule.findall('.//event'):
        conf_url = event.findtext('conf_url')
        if eventid_value == 'scheduleitem':
            id_ = event.attrib['id']
        elif eventid_value == 'slug':
            id_ = event.findtext('slug')
            conf_url = event.findtext('url')
        elif 'guid' in event.attrib:
            id_ = event.attrib['guid']
            conf_url = event.findtext('url')
        else:
            m = event_id_re.match(conf_url)
            id_ = None
            if m:
                id_ = m.group(1)
        if id_:
            by_id[id_] = {
                'url': conf_url,
                'description': event.findtext('description')
            }
    return by_id


def debconf(meta, wafer_base_url, eventid_value):
    if not wafer_base_url:
        return
    locations = {
        20: 'The Internet',
    }
    pentabarf_url = urljoin(wafer_base_url, 'schedule/pentabarf.xml')
    schedule = read_dc_schedule(pentabarf_url, eventid_value)
    m = re.match(r'^https://debconf(\d+)\.debconf\.org/$', wafer_base_url)
    if m:
        edition = int(m.group(1))
        meta.conference.series = 'DebConf'
        meta.conference.edition = str(edition)
        meta.conference.location = locations.get(edition, 'UNKNOWN')
        meta.conference.website = wafer_base_url
        meta.conference.schedule = urljoin(wafer_base_url, '/schedule/')
        meta.conference.video_base = (
            'https://meetings-archive.debian.net/pub/debian-meetings/'
            '{}/DebConf{}/'.format(2000 + edition, edition))

    for video in meta.videos:
        conf_url = video.description  # Hack, we stashed this there
        if conf_url in schedule:
            sched_data = schedule[video.description]
            video.description = sched_data['description']
            video.details_url = urljoin(wafer_base_url, sched_data['url'])
        else:
            del video.description


def fixup(json, wafer_base_url):
    # We parsed as JSON, dates weren't parsed
    json['conference']['date'] = [
        parse_date(datestr) for datestr in json['conference']['date']]

    for video in json['videos']:
        if wafer_base_url:
            # temporarily stash ID in description, so we can find it later
            video['description'] = video.pop('eventid')
        else:
            del video['eventid']
        if video['description'] is None:
            del video['description']
        else:
            description = video['description'].strip()
            video['description'] = re.sub(r'\s*\n\s*', '\n', description)

        video['speakers'] = list(split_speakers(video['speakers']))
        if video['speakers'] == ['']:
            del video['speakers']
        video['start'] = parse_datetime(video['start'])
        video['end'] = parse_datetime(video['end'])

        video_url = video['video']
        # Strip prefix dir, we can include it in video_base
        video_url = video_url.split('/', 1)[1]
        if video_url.endswith('.lq.webm'):
            video['alt_formats'] = {
                'lq': video_url,
            }
            video_url = video_url.replace('.lq.webm', '.webm')
        else:
            video['alt_formats'] = {
                'lq': video_url.rsplit('.', 1)[0] + '.lq.webm',
            }
        video['video'] = video_url

    json['videos'].sort(key=lambda v: (v['start'], v['room']))

    return json


def re_serialize(meta, path):
    if all(hasattr(event, 'start') for event in meta.videos):
        meta.videos.sort(key=lambda event: event.start)

    tmp_path = path.with_name('.new')
    with tmp_path.open('w', encoding='utf8') as f:
        try:
            yaml_dump(meta, f)
            f.flush()
            os.fsync(f.fileno())
            os.replace(str(tmp_path), str(path))
        finally:
            if tmp_path.exists():
                tmp_path.unlink()


def main():
    parser = argparse.ArgumentParser(
        description='Download sreview JSON, save as YAML')
    parser.add_argument('output', help='File to produce')
    parser.add_argument(
        '-u', '--url', default='https://sreview.debian.net/released',
        help='SReview /released URL')
    parser.add_argument(
        '-w', '--wafer', help='Wafer conference site URL')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--scheduleitem-ids',
                       action='store_const', dest='eventid_value',
                       const='scheduleitem', default='talk',
                       help='SReview stored Schedule Item IDs as eventid, '
                            'rather than Talk ID')
    group.add_argument('--slugs', action='store_const', dest='eventid_value',
                       const='slug',
                        help='SReview stored Event slugs as eventid, '
                             'rather than Talk ID')
    args = parser.parse_args()

    if args.wafer:
        args.wafer = args.wafer.rstrip('/') + '/'

    r = requests.get(args.url)
    if r.status_code != 200:
        raise Exception('HTTP {}'.format(r.status_code))
    json = r.json()

    json = fixup(json, args.wafer)
    meta = load_meta_from_dict(json)

    debconf(meta, args.wafer, args.eventid_value)

    re_serialize(meta, Path(args.output))


if __name__ == '__main__':
    main()
